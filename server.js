const express = require('express');
const http = require('http');
const bodyParser = require('body-parser');
const app = express();
const router = require('./router');
const mongoose = require('mongoose');
const cors = require('cors');

require('dotenv').config(); 


mongoose.connect(process.env.database);
mongoose.connection.on('connected',() => {
	console.log('connected in database success'); 
});
mongoose.connection.on('error',() => {
	throw new Error('error connected in database'); 
})

app.use(cors());
app.use(bodyParser.json({limit: "50mb"}));

router(app);
const port = process.env.PORT || 8080;
const server = http.createServer(app);
server.listen(port);
console.log('Server listening on: ', port);