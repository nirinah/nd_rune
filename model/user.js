const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema({
  email: { type: String, unique: true, lowercase: true },
  firstName: String,
  lastName: String,
  phone: String,
  addresse: { type: String, default: '' },
  avatar: { type: String, default: '' },
});

const ModelClass = mongoose.model('user', userSchema);
module.exports = ModelClass;