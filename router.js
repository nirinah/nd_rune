const Post = require('./controller/users');

module.exports = function(app) {
	app.get('/', Post.Home);
	app.get('/api/users',Post.fetchUsers);
  	app.post('/api/create/user',Post.createUser);
  	app.get('/api/user/:id',Post.fetchUser);
  	app.put('/api/user/:id',Post.updateUser);
  	app.delete('/api/user/:id',Post.deleteUser);
}