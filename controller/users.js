const User = require('../model/user');
const fs = require("fs");
const admin = require("firebase-admin");
const serviceAccount = require("../config/bootcamp-users-firebase-adminsdk-hhd2v-51c07fc1e9.json");
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  storageBucket: "bootcamp-users.appspot.com"
});

/*var firebaseConfig = {
  apiKey: "AIzaSyBTfALDPi0e4EP_qj3tuVHuPU9dnRc_RXY",
  authDomain: "bootcamp-users.firebaseapp.com",
  databaseURL: "https://bootcamp-users.firebaseio.com",
  projectId: "bootcamp-users",
  storageBucket: "bootcamp-users.appspot.com",
  messagingSenderId: "219602182410",
  appId: "1:219602182410:web:bc3d38491ce9a61be81af3",
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);*/


/**
 * Create a new comment (post ID and user ID are both needed)
 *
 * @param req
 * @param res
 * @param next
 */
exports.createUser = function(req, res, next) {
  let user;
  let fileName = "";
  const file = req.body.avatar;
	const email = req.body.email;
  const firstName = req.body.firstName;
	const lastName = req.body.lastName;
  const phone = req.body.phone;
  const addresse = req.body.adresse;

  if (!email && !lastName && !phone) {
  	return res.json({
      success : false,
      code: 400,
      message: "You must provide both email and lastName phone."
    });
  } 

  // See if a user with given email exists
  User.findOne({ email: email }, function(err, existingUser) {
    if (err) {
      return next(err);
    }
    // If a user with email does exist, return an error
    if (existingUser) {
      return res.json({
        success: false,
        code: 400,
        message: "email already existe"
      });
    }
    user = {
      email: email,
      firstName: firstName,
      lastName: lastName,
      phone:phone,
      addresse:addresse,
      avatar:""
    }
    fileName = setFileManager(file,lastName,firstName);
    if(fileName) {
      const myFile = admin.storage().bucket().file(`image/${fileName}`);
      myFile.getSignedUrl({action: 'read', expires: "2020-12-30"}).then(urls => {
        user.avatar = urls[0];
        UserManager(user);
      })
    } else {
      UserManager(user);
    }

    res.json({
      code: 201,
      message: "user save with success", 
    });
  });
}

function UserManager(user){
  user = new User(user);
  user.save(function(err) {  
      if (err) {
        return next(err);
      }
    });
    return;
}

/**
 * Fecth User
 *
 * @param req
 * @param res
 * @param next
 */
exports.fetchUsers = function(req, res, next) {
  
	User
    .find({})
    .select({})
    .limit(100)
    .sort({
      time: -1
    })
    .exec(function(err, posts) {
      if (err) {
        return res.json({
	          success : false,
	          code : 400,
	          message: "error"
      	});
      }
      res.json({
         code:200,
         message: 'sucess',
         data: posts
       });
    });
}

/**
 * 
 *
 * @param req
 * @param res
 * @param next
 */
exports.fetchUser = function(req, res, next) {
	User.findById({ _id: req.params.id }, function(err, resp) {
		if (err) {
			return res.json({
              success : false,
              code: 400,
              message: "error interne"
          });
		}
    
		res.json({
      success : true,
      code : 200,
      data:resp
    });
	})
}

/**
 * 
 *
 * @param req
 * @param res
 * @param next
 */
exports.updateUser = function(req, res, next) {
  let path = "";
  User.findById({
    _id: req.params.id
  }, function(err, post) {

    if (err) {
      res.json({
      	success : false,
      	code : 400,
      	message: "user not found"
      })
    }

    // Check if the post exist
    if (!post) {
      	res.json({
              success : false,
              code : 400,
              message: "user not found"
      	})
    }

    const email = req.body.email;
  	const firstName = req.body.firstName;
	  const lastName = req.body.lastName;
  	const phone = req.body.phone;
    const addresse = req.body.addresse;
    const avatar = req.body.avatar;
    
    if (email) {
      post.email = email;
    }
    if (firstName) {
      post.firstName = firstName;
    }
    if (lastName) {
      post.lastName = lastName;
    }
    if (phone) {
      post.phone = phone;
    }

    if (addresse) {
      post.addresse = address;
    }

    if (avatar) {
      path = setFileManager(avatar, post.firstName, post.lastName);
      post.avatar = path;
    }
    //post.lastName = lastName;

    post.save(function(err, post) { 
      if (err) {
        return next(err);
      }
     	res.json({
            success : true,
            code : 200,
            message:"user update with success"
        });
    });
  });
}

/**
 * 
 *
 * @param req
 * @param res
 * @param next
 */
exports.deleteUser = function(req, res, next) {
	User.findByIdAndRemove(req.params.id, function(err, user) {
		if (err) {
      		return next(err);
	    }
	    if (!user) {
	      return res.json({
	        success: false,
	        code: 400,
	        message: "user not found",
	      });
	    }
	    // Return a success message
	    res.json({
	      success: true,
	      code: 200,
	      message: "user delete with success",
	    });
  });
  
}

exports.Home = function (req, res, next) {
  res.json({
    success: true,
    code: 200,
    message: "Hello App training master",
  });
}

const setTypeFile = (resp) => {
  let file = "";

  if (resp && fs.existsSync(resp)) {
    const type = resp.split(".")[1];
    const af = fs.readFileSync(resp);
    if (type == "jpeg") {
       data = "data:image/jpeg;base64,";
     } else if (type == "jpg") {
       data = "data:image/jpg;base64,";
     } else if (type == "png") {
       data = "data:image/png;base64,";
     }
    file = data + (new Buffer(af).toString('base64'));
  }

  return file
}

function setFileManager(file, lastName, firstName){
    if (!file) {
      return;
    }

    const date = new Date();
    let data;
    const bucket = admin.storage().bucket();
    const dirpath = "image/";
    
    if (!fs.existsSync(dirpath)) {
        fs.mkdirSync(dirpath, { recursive: true })
    } 
    const nameFile = lastName + "_" + firstName + "_" + date.getDate() + date.getMonth() + date.getFullYear()+date.getSeconds() +"." + file.type;
    let path = dirpath+ nameFile; 
    
    if (file.type == "jpeg") {
      data = file.data.replace(/^data:image\/jpeg;base64,/, "");
    } else if (file.type == "") {
      data = file.data.replace(/^data:image\/jpg;base64,/, "");
    } else if (file.type == "png") {
      data = file.data.replace(/^data:image\/png;base64,/, "");
    }
    imageBuffer = new Buffer(data, 'base64');
    const bckRef = bucket.file(path);
    bckRef.save(imageBuffer, {
      metadata: { contentType: `image/${file.type}` },
      predefinedAcl: "publicRead",
      public: true,
    });

    return nameFile;

    // const uri = storage.refFromURL(`https://firebasestorage.googleapis.com/b/bucket/o/image%${nameFile}`);
    // const uri = `https://firebasestorage.googleapis.com/v0/b/bootcamp-users.appspot.com/o/image/${encodeURIComponent(nameFile)}?alt=media&token=${uuid}`;
    //return uri;
}

